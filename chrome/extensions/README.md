# 谷歌浏览器扩展

https://chrome.google.com/webstore

## 必备

[谷歌访问助手](http://www.ggfwzs.com/) 2.3.0

## 网站

### 中文

http://www.cnplugins.com/

http://chromecj.com/

### 英文

http://www.chromeextensions.org/

## 开发

[【干货】Chrome插件(扩展)开发全攻略](https://www.cnblogs.com/liuxianan/p/chrome-plugin-develop.html)

[如何创建并发布一个 Chrome 扩展应用](https://juejin.im/entry/5a9810d3f265da239f06cb32)

https://crxdoc-zh.appspot.com/extensions/devguide

